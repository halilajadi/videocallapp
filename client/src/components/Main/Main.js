import React, { useRef, useState, useEffect } from 'react';
import styled from 'styled-components';
import logo from '../../assets/img/smart-call-logo.svg';
import socket from '../../socket';

const Main = (props) => {
  const roomRef = useRef();
  const userRef = useRef();
  const [err, setErr] = useState(false);
  const [errMsg, setErrMsg] = useState('');

  useEffect(() => {

    socket.on('FE-error-user-exist', ({ error }) => {
      if (!error) {
        const roomName = roomRef.current.value;
        const userName = userRef.current.value;

        sessionStorage.setItem('user', userName);
        props.history.push(`/room/${roomName}`);
      } else {
        setErr(error);
        setErrMsg('User name already exist');
      }
    });
  }, [props.history]);

  function clickJoin() {
    const roomName = roomRef.current.value;
    const userName = userRef.current.value;

    if (!roomName || !userName) {
      setErr(true);
      setErrMsg('Enter Room Name or User Name');
    } else {
      socket.emit('BE-check-user', { roomId: roomName, userName });
    }
  }

  return (
    <MainContainer className="vc-home w-100">
       <div className="container-fluid">
          <div className="row vh-100">
            <div className="col-12 col-md-3 mt-4">
              <a className="btn-main" href="https://smartbusinesscircle-module.com">
                  Back
              </a>
            </div>
            <div className="col-12 col-md-6">
              <Row>
                <div>
                  <h2 className="wc-home-title">
                    Premium video metings.
                  </h2>
                  <h2 className="wc-home-title">
                    Now free for everyone.
                  </h2>
                </div>
              </Row>
              <div className="login-form-user">
                <div className="login-form-user-wrapper">
                  <h3>SMART CALL</h3>
                  <Row>
                    <Input className="input-main" type="text" id="roomName" placeholder="ROOM NAME" ref={roomRef} />
                  </Row>
                  <Row>
                    <Input className="input-main" type="text" id="userName" placeholder="USER NAME" ref={userRef} />
                  </Row>
                  <JoinButton className="btn" onClick={clickJoin}> Start </JoinButton>
                  {err ? <Error>{errMsg}</Error> : null}
                </div>
              </div>
            </div>
            <div className="col-12 col-md-3 mt-4">
              <img className="img-fluid" src={logo} alt="Logo smart call" />
            </div>
          </div>
      </div>
    </MainContainer>
  );
};

const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const Row = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 15px;
  line-height: 35px;
`;

const Label = styled.label``;

const Input = styled.input`
  width: 350px;
  height: 45px;
  outline: none;
  padding-bottom: 10px;
  color: #fff;
  border-bottom: 2px solid #fff;
  background-color: transparent;
`;

const Error = styled.div`
  margin-top: 10px;
  font-size: 20px;
  color: #e85a71;
`;

const JoinButton = styled.button`
  margin-top: 35px;
  outline: none;
  border: none;
  border-radius: 30px;
  color: #000;
  font-size: 25px;
  font-weight: 200;
  padding: 10px 30px;
  border: none;
  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.9);
  background-color: #fff;
  text-transform: uppercase;

  :hover {
    color: #f2f2f2;
    background-color: rgba(19, 22, 99, 0.3);
    cursor: pointer;
  }
`;

export default Main;
