import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Main from './components/Main/Main';
import Room from './components/Room/Room';
import styled from 'styled-components';
import './assets/scss/style.css';

function App() {
  return (
    <BrowserRouter>
      <AppContainer>
        <Switch>
          <Route exact path="/" component={Main} />
          <Route exact path="/room/:roomId" component={Room} />
        </Switch>
      </AppContainer>
    </BrowserRouter>
  );
}

const AppContainer = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  align-items: center;
  justify-content: center;
  font-size: calc(8px + 2vmin);
  color: white;
  background-image: linear-gradient(
    180deg,rgba(19,22,99,0.24) 0%,rgba(0,0,0,0) 100%);
  background-color: RGBA(0,0,0,0);
  backdrop-filter: blur(15px);
  -webkit-backdrop-filter: blur(15px);
  text-align: center;
`;

export default App;
